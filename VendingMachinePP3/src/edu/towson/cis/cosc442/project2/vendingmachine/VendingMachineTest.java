package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineTest {
	//Requirements: 
	//1. need a vending machine to play w 
	//2. need vending machine items to play w
	VendingMachine myVendingMachine;
	VendingMachineItem chips;
	VendingMachineItem candy; 
	VendingMachineItem soda; 

	
	@Before
	public void setUp() throws Exception {
		//1. need to create constructor 
		myVendingMachine = new VendingMachine();
		//2. now u set the items to what they hold (name, and price) 
		chips = new VendingMachineItem("Fritos",2.0); 
		candy = new VendingMachineItem("Gum", 0.25);
		soda = new VendingMachineItem("coke", 0.025); 
	}

	@After
	public void tearDown() throws Exception {
	}

	
	
	@Test 
	//a test to test getSlotIndex 
	public void testgetSlotIndexA() {
		myVendingMachine.addItem(chips, "A");
		VendingMachineItem item = myVendingMachine.getItem("A"); 
		assertEquals(item, chips); 
	}
	@Test 
	//a test to test getSlotIndex 
	public void testgetSlotIndexB() {
		myVendingMachine.addItem(chips, "B");
		VendingMachineItem item = myVendingMachine.getItem("B"); 
		assertEquals(item, chips); 
	}
	@Test 
	//a test to test getSlotIndex 
	public void testgetSlotIndexC() {
		myVendingMachine.addItem(chips, "C");
		VendingMachineItem item = myVendingMachine.getItem("C"); 
		assertEquals(item, chips); 
	}
	
	@Test 
	//a test to test getSlotIndex 
	public void testgetSlotIndexD() {
		myVendingMachine.addItem(chips, "D");
		VendingMachineItem item = myVendingMachine.getItem("D"); 
		assertEquals(item, chips); 
	}
	@Test 
	//a test to test the thrown error in getSlotIndex 
	public void testgetSlotIndexException() {
		  boolean thrown = false;
		  try {
		  VendingMachineItem item = myVendingMachine.getItem("E"); 
		  } catch (VendingMachineException e) {
		    thrown = true;
		  }
		  assertTrue(thrown);
		}
		
		
		

	@Test 
	//a test to test that adding item works 
	public void testaddItem() { 
		myVendingMachine.addItem(chips, "A");
		VendingMachineItem item = myVendingMachine.getItem("A");
		assertEquals(item, chips); 
	}
	

	@Test (expected = VendingMachineException.class)
	//a test to test that adding item works when an item is already in it 
	public void testaddItemExecption() { 
		boolean thrown = false;
		myVendingMachine.addItem(chips, "A");
		myVendingMachine.addItem(candy, "A");
		    thrown = true;
		  }

	@Test (expected = VendingMachineException.class)
	//a test to test that adding item works when the item code is invaild 
	public void testaddItemExecption2() { 
		boolean thrown = false;
		myVendingMachine.addItem(chips, "Z");
		    thrown = true;
	}

	@Test
	//a test to see if removing an item is working 
	public void testremoveItem() { 
		myVendingMachine.addItem(chips, "A");
		VendingMachineItem item = myVendingMachine.removeItem("A");
		assertEquals(item, chips);
	}

	@Test 
	//a test to see if inserting money into the vending machine is working 
	public void testinsertMoney() { 
		myVendingMachine.insertMoney(5.00);
		assertEquals(5.00, myVendingMachine.getBalance(), .001); 
	}
	
	@Test (expected = VendingMachineException.class)
	//a test to see if inserting money into the vending machine exception is working 
		public void testinsertMoneyExecption() { 
			  myVendingMachine.insertMoney(-50.00);
	}

	@Test 
	//a test to see if getting the balance is working 
	public void getBalance() { 
	myVendingMachine.getBalance();
	assertEquals(0.00, myVendingMachine.getBalance(), .001);	
	}
	
	@Test 
	//a test to make sure making a Purchase is correct 
	public void makePurchase() { 
		 myVendingMachine.makePurchase("A"); 
		 assertFalse(myVendingMachine.makePurchase("A"));  
	} 
	
	@Test (expected = VendingMachineException.class)
	//a test to see if removing an item expection is working when removing a null item 
	public void testremoveItemException() { 
		VendingMachineItem item = myVendingMachine.removeItem("Z");
	}

	@Test (expected = VendingMachineException.class)
	//a test to see if removing item is working when this.balance >= item.getPrice
	public void testremoveException2() { 
		myVendingMachine.getBalance();
		
		VendingMachineItem item = myVendingMachine.removeItem("A");		
	}
	

	@Test
	//a test to make sure we are getting the correct change returned to user
		public void returnChange() { 
		double change = myVendingMachine.returnChange();
		assertEquals(change, myVendingMachine.returnChange(), .001);
	}
	
	@Test 
	//TRYING a test to make sure making a Purchase is correct 
	public void makePurchaseExceptionA() { 
			//adding an item with a very low price ; meaning != null 
			myVendingMachine.addItem(candy, "C");
			//now getting the balance of the item (candy in slot C that has price of 0.25 
			myVendingMachine.getBalance(); 
			myVendingMachine.makePurchase("C"); 
		} 
	
	@Test 
	//TRYING a test to make sure making a Purchase is correct 
	public void makePurchaseExceptionB() { 
			//if item is null 
//			myVendingMachine.addItem(soda, null);
			//now getting the balance of the item (candy in slot C that has price of 0.25 
			myVendingMachine.getBalance(); 
			myVendingMachine.makePurchase("D"); 
			assertFalse(myVendingMachine.makePurchase("A"));  
		} 
	
	@Test 
	//TRYING a test to make sure making a Purchase is correct 
	public void makePurchaseExceptionC() { 
		//adding an item to get balance of 
		myVendingMachine.insertMoney(5.00);
		myVendingMachine.getBalance(); 
		assertFalse(myVendingMachine.makePurchase("A"));
		} 
	@Test 
	//a test to make sure making a Purchase is correct 
	public void makePurchaseSuccessD() { 
		myVendingMachine.addItem(candy, "A");
		myVendingMachine.insertMoney(1.0);
		assertTrue(myVendingMachine.makePurchase("A"));
	} 
	}
