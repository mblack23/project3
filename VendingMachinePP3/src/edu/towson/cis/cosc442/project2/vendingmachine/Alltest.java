package edu.towson.cis.cosc442.project2.vendingmachine;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ VendingMachineItemTest.class, VendingMachineTest.class })
public class Alltest {
	//this file runs all of the test cases inside of VendingMachine 
}
