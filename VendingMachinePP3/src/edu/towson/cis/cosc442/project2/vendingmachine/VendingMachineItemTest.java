package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineItemTest {
	//Req: 
	//1. need a vending machine to play w 
	//2. need vending machhine items to play w
	VendingMachine myVendingMachine;
	VendingMachineItem chips;
	VendingMachineItem candy;

	
	@Before
	public void setUp() throws Exception {
		//1. need to create constructor 
		myVendingMachine = new VendingMachine();
		//2. now u set the items to what they hold (name, and price) 
		chips = new VendingMachineItem("Fritos",2.0); 

	}

	@After
	public void tearDown() throws Exception {
		//clean up method 
		//after it runs the test it is going to clean / undo everything 
		//typically stuff wont need stuff in tear down 
		// myVendingMachine = null; is an example but until told other wise nothing is needed 
	}

	@Test
	//a test to get the name of the items 
	public void testgetName() {
		assertEquals(chips.getName(),"Fritos"); 

	}
	@Test
	//a test to get the price of an item 
	public void testgetPrice() {
		assertEquals(chips.getPrice(),2.0, .00001);
	}
	@Test 
	//a test to get test that the VendingMachineItem constructor is working 
	public void testVendingMachineItem() { 
		assertEquals(chips.getName(),"Fritos"); 
		assertEquals(chips.getPrice(),2.0, .00001);
	}

	@Test (expected = VendingMachineException.class)
	//a test to get test that the VendingMachineItem constructor is working 
	public void testVendingMachineItemA() { 
		candy = new VendingMachineItem("Gum", -1.00); 
		assertEquals(candy.getName(),"Gum"); 
		assertEquals(candy.getPrice(),-1.00, .00001);

	}
} 



